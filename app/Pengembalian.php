<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengembalian extends Model
{
    protected $primaryKey = 'id_pengembalian';
    protected $fillable = [
        'penyewaan_id',
        'tanggal_pengembalian',
        'keterangan',
        'status'
    ];

    public function penyewaan(){
        return $this->belongsTo('App\Penyewaan', 'penyewaan_id');
    }
}

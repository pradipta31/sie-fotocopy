<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fotocopy extends Model
{
    protected $primaryKey = 'id_fotocopy';
    protected $fillable = [
        'merk',
        'tahun',
        'harga',
        'gambar',
        'spesifikasi',
        'qty',
        'status'
    ];
}

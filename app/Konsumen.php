<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Konsumen extends Model
{
    protected $primaryKey = 'id_konsumen';
    protected $fillable = [
        'no_ktp',
        'foto_ktp',
        'nama',
        'tempat_lahir',
        'tanggal_lahir',
        'email',
        'alamat',
        'no_telp',
        'status'
    ];
}

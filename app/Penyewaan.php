<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penyewaan extends Model
{
    protected $primaryKey = 'id_penyewaan';
    protected $fillable = [
        'user_id',
        'konsumen_id',
        'fotocopy_id',
        'tanggal_transaksi',
        'bukti_pembayaran',
        'qty',
        'total',
        'keterangan',
        'status'
    ];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    public function konsumen(){
        return $this->belongsTo('App\Konsumen','konsumen_id');
    }

    public function fotocopy(){
        return $this->belongsTo('App\Fotocopy','fotocopy_id');
    }
}

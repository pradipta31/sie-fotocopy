<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Konsumen;
use App\Fotocopy;
use App\Pengembalian;
use App\Penyewaan;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $monthOrder = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

        $countPane = [
            'konsumens' => Konsumen::all()->count(),
            'fotocopies' => Fotocopy::all()->count(),
            'pengembalians' => Pengembalian::all()->count(),
            'penyewaans' => Penyewaan::all()->count()
        ];

        $dataChart = null;

        $penyewaans = Penyewaan::whereYear('tanggal_transaksi', Carbon::now()->year)->orderBy('tanggal_transaksi', 'ASC')->get();
        $penyewaanPerMonth = $penyewaans->groupBy(function ($item, $key) {
            return Carbon::parse($item['tanggal_transaksi'])->monthName;
        })->map->count();

        $pengembalian = Pengembalian::whereYear('tanggal_pengembalian', Carbon::now()->year)->orderBy('tanggal_pengembalian', 'ASC')->get();
        $pengembalianPerMonth = $pengembalian->groupBy(function ($item, $key) {
            return Carbon::parse($item['tanggal_pengembalian'])->monthName;
        })->map->count();

        $months = $penyewaanPerMonth->merge($pengembalianPerMonth);
        $orderedMonths = array_values(array_intersect($monthOrder, $months->keys()->toArray()));

        $monthsPengembalianDiff = array_diff($orderedMonths, $pengembalianPerMonth->keys()->toArray());
        foreach ($monthsPengembalianDiff as $key => $value) {
            $pengembalianPerMonth[$value] = 0;
        }
        $pengembalianPerMonth = collect(array_merge(array_flip($orderedMonths), $pengembalianPerMonth->toArray()));
        // [END] Check bulan dan diurutkan setiap bulan Pengembalian
        
        // Penyewaan
        $monthsPenyewaanDiff = array_diff($orderedMonths, $pengembalianPerMonth->keys()->toArray());
        foreach ($monthsPenyewaanDiff as $key => $value) {
            $penyewaanPerMonth[$value] = 0;
        }
        $penyewaanPerMonth = collect(array_merge(array_flip($orderedMonths), $penyewaanPerMonth->toArray()));
        // [END] Check bulan dan diurutkan setiap bulan Penyewaan
        
        $dataChart = [
            'months' => $orderedMonths,
            'penyewaan' => $penyewaanPerMonth,
            'pengembalian' => $pengembalianPerMonth,
        ];

        return view('pimpinan.dashboard', compact('dataChart'),$countPane);
    }
}

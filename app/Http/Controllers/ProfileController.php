<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use Hash;
use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index(){
        $user = User::where('id_user', Auth::user()->id_user)->first();
        return view('profile', compact('user'));
    }

    public function update(Request $r, $id){
        $v = Validator::make($r->all(), [
            'nama' => 'required',
            'username' => 'required',
            'email' => 'required',
            'no_telp' => 'required|numeric'
        ]);

        if ($v->fails()) {
            toastError($v->messages()->first());
            return redirect()->back()->withInput();
        }else{
            if ($r->password != null) {
                $users = User::where('id_user',$id)->update([
                    'nama' => $r->nama,
                    'username' => $r->username,
                    'email' => $r->email,
                    'password' => Hash::make($r->password),
                    'no_telp' => $r->no_telp
                ]);
                toastr()->success('Profile anda berhasil diubah.');
                return redirect(url('pimpinan/user'));
            }else{
                $users = User::where('id_user',$id)->update([
                    'nama' => $r->nama,
                    'username' => $r->username,
                    'email' => $r->email,
                    'no_telp' => $r->no_telp
                ]);
                toastr()->success('Profile anda berhasil diubah.');
                return redirect()->back();
            }
        }
    }
}

<?php

namespace App\Http\Controllers\Pimpinan;

use App\Fotocopy;
use App\Konsumen;
use App\Penyewaan;
use App\Pengembalian;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MasterController extends Controller
{
    public function indexFotocopy(){
        $fotocopies = Fotocopy::all();
        return view('pimpinan.fotocopy.index', compact('fotocopies'));
    }

    public function indexKonsumen(){
        $konsumens = Konsumen::all();
        return view('pimpinan.konsumen.index', compact('konsumens'));
    }

    public function indexPengembalian(){
        $pengembalians = Pengembalian::all();
        return view('pimpinan.pengembalian.index', compact('pengembalians'));
    }

    public function indexPenyewaan(){
        $penyewaans = Penyewaan::all();
        return view('pimpinan.penyewaan.index', compact('penyewaans'));
    }

    // FUNCTION DOWNLOAD DOWN HERE
    public function downloadKonsumen(){
        $spreadsheet = new Spreadsheet();
        $konsumens = Konsumen::all();

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'No');
        $sheet->setCellValue('B1', 'No KTP');
        $sheet->setCellValue('C1', 'Nama');
        $sheet->setCellValue('D1', 'Email');
        $sheet->setCellValue('E1', 'Alamat');
        $sheet->setCellValue('F1', 'Tempat Lahir');
        $sheet->setCellValue('G1', 'Tanggal Lahir');
        $sheet->setCellValue('H1', 'Nomor Telepon');
        $sheet->setCellValue('I1', 'Tanggal Didaftarkan');

        $row = 2;
        $nomor = 1;
        foreach($konsumens as $konsumen){
            $sheet->setCellValue('A'.$row,$nomor++);
            $sheet->setCellValue('B'.$row,$konsumen->no_ktp);
            $sheet->setCellValue('C'.$row,$konsumen->nama);
            $sheet->setCellValue('D'.$row,$konsumen->email);
            $sheet->setCellValue('E'.$row,$konsumen->alamat);
            $sheet->setCellValue('F'.$row,$konsumen->tempat_lahir);
            $sheet->setCellValue('G'.$row,$konsumen->tanggal_lahir);
            $sheet->setCellValue('H'.$row,$konsumen->no_telp);
            $sheet->setCellValue('I'.$row,$konsumen->created_at->format('d-m-Y'));
            $row++;
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save('download.xlsx');
        return response()->download(public_path('download.xlsx'))->deleteFileAfterSend();
    }

    public function downloadFotocopy(){
        $spreadsheet = new Spreadsheet();
        $fotocopies = Fotocopy::all();

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'No');
        $sheet->setCellValue('B1', 'Merk');
        $sheet->setCellValue('C1', 'Tahun');
        $sheet->setCellValue('D1', 'Harga');
        $sheet->setCellValue('E1', 'Spesifikasi');
        $sheet->setCellValue('F1', 'Status');
        $sheet->setCellValue('G1', 'Tanggal Didaftarkan');

        $row = 2;
        $nomor = 1;
        foreach($fotocopies as $fotocopy){
            $sheet->setCellValue('A'.$row,$nomor++);
            $sheet->setCellValue('B'.$row,$fotocopy->merk);
            $sheet->setCellValue('C'.$row,$fotocopy->tahun);
            $sheet->setCellValue('D'.$row,$fotocopy->harga);
            $sheet->setCellValue('E'.$row,$fotocopy->spesifikasi);
            $sheet->setCellValue('F'.$row,$fotocopy->status);
            $sheet->setCellValue('G'.$row,$fotocopy->created_at->format('d-m-Y'));
            $row++;
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save('fotocopyreport.xlsx');
        return response()->download(public_path('fotocopyreport.xlsx'))->deleteFileAfterSend();
    }

    public function downloadPenyewaan(){
        $spreadsheet = new Spreadsheet();
        $penyewaans = Penyewaan::all();

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'No');
        $sheet->setCellValue('B1', 'Konsumen');
        $sheet->setCellValue('C1', 'Merk Fotocopy');
        $sheet->setCellValue('D1', 'Tanggal Penyewaan');
        $sheet->setCellValue('E1', 'Keterangan');
        $sheet->setCellValue('F1', 'Status');

        $row = 2;
        $nomor = 1;
        foreach($penyewaans as $penyewaan){
            $sheet->setCellValue('A'.$row,$nomor++);
            $sheet->setCellValue('B'.$row,$penyewaan->konsumen->nama);
            $sheet->setCellValue('C'.$row,$penyewaan->fotocopy->merk);
            $sheet->setCellValue('D'.$row,$penyewaan->tanggal_transaksi);
            $sheet->setCellValue('E'.$row,$penyewaan->keterangan);
            $sheet->setCellValue('F'.$row,$penyewaan->status);
            $row++;
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save('penyewaanreport.xlsx');
        return response()->download(public_path('penyewaanreport.xlsx'))->deleteFileAfterSend();
    }

    public function downloadPengembalian(){
        $spreadsheet = new Spreadsheet();
        $pengembalians = Pengembalian::all();

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'No');
        $sheet->setCellValue('B1', 'Konsumen');
        $sheet->setCellValue('C1', 'Merk Fotocopy');
        $sheet->setCellValue('D1', 'Tanggal Penyewaan');
        $sheet->setCellValue('E1', 'Tanggal Pengembalian');
        $sheet->setCellValue('F1', 'Keterangan');
        $sheet->setCellValue('G1', 'Status');

        $row = 2;
        $nomor = 1;
        foreach($pengembalians as $pengembalian){
            $sheet->setCellValue('A'.$row,$nomor++);
            $sheet->setCellValue('B'.$row,$pengembalian->penyewaan->konsumen->nama);
            $sheet->setCellValue('C'.$row,$pengembalian->penyewaan->fotocopy->merk);
            $sheet->setCellValue('D'.$row,$pengembalian->penyewaan->tanggal_transaksi);
            $sheet->setCellValue('E'.$row,$pengembalian->tanggal_pengembalian);
            $sheet->setCellValue('F'.$row,$pengembalian->keterangan);
            $sheet->setCellValue('G'.$row,$pengembalian->status);
            $row++;
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save('pengembalianreport.xlsx');
        return response()->download(public_path('pengembalianreport.xlsx'))->deleteFileAfterSend();
    }
}

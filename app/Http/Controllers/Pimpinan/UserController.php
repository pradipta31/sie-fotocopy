<?php

namespace App\Http\Controllers\Pimpinan;

use Hash;
use Validator;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function create(){
        return view('pimpinan.user.create');
    }

    public function index(){
        $users = User::all();
        return view('pimpinan.user.index', compact('users'));
    }

    public function store(Request $r){
        $v = Validator::make($r->all(), [
            'nama' => 'required',
            'username' => 'required|unique:users',
            'email' => 'required',
            'password' => 'required',
            'no_telp' => 'required|numeric',
            'level' => 'required'
        ]);

        if ($v->fails()) {
            toastError($v->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $users = User::create([
                'nama' => $r->nama,
                'username' => $r->username,
                'email' => $r->email,
                'password' => Hash::make($r->password),
                'level' => $r->level,
                'no_telp' => $r->no_telp,
                'status' => 1
            ]);
            toastr()->success('User baru berhasil ditambahkan!');
            return redirect(url('pimpinan/user'));
        }
    }

    public function changePassword(Request $r, $id){
        $user = User::where('id_user',$id)->update([
            'password' => Hash::make($r->password)
        ]);
        toastSuccess('Password berhasil diubah!');
        return redirect()->back();
    }

    public function changeActive($id){
        $user = User::where('id_user',$id)->update([
            'status' => 1
        ]);
        toastSuccess('User berhasil diaktifkan!');
        return redirect()->back();
    }

    public function changeInnactive($id){
        $user = User::where('id_user',$id)->update([
            'status' => 0
        ]);
        toastWarning('User dinonaktifkan!');
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Validator;
use Image;

use App\User;
use App\Konsumen;
use App\Penyewaan;
use App\Fotocopy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PenyewaanController extends Controller
{
    public function create(){
        $konsumens = Konsumen::where('status',1)->get();
        $fotocopies = Fotocopy::where('status','ready')->get();
        return view('admin.penyewaan.create', compact('konsumens','fotocopies'));
    }

    public function store(Request $r){
        $validator = Validator::make($r->all(), [
            'id_konsumen' => 'required',
            'id_fotocopy' => 'required',
            'tanggal_transaksi' => 'required',
            'bukti_pembayaran' => 'required|image|mimes:jpeg,png,jpg|max:5024',
            'qty' => 'required',
            'keterangan' => 'required'
        ]);
        if (!$validator->fails()) {
            $fotocopy = Fotocopy::where('id_fotocopy',$r->id_fotocopy)->first();
            if ($r->qty > $fotocopy->qty) {
                // dd('Quantity tidak boleh melebihi stock!');
                toastr()->error('Qty tidak boleh melebihi stock!');
                return redirect()->back()->withInput();
            }else{
                // dd('berhasil!');
                $qty = $fotocopy->qty - $r->qty;
                $hrg = $fotocopy->harga * $r->qty;
                $harga = number_format($hrg,2);
                // dd($harga);
                $bukti = $r->file('bukti_pembayaran');
                $filename = time() . '.' . $bukti->getClientOriginalExtension();
                Image::make($bukti)->save(public_path('/images/bukti/'.$filename));
                $penyewaan = Penyewaan::create([
                    'user_id' => Auth::user()->id_user,
                    'konsumen_id' => $r->id_konsumen,
                    'fotocopy_id' => $r->id_fotocopy,
                    'tanggal_transaksi' => $r->tanggal_transaksi,
                    'bukti_pembayaran' => $filename,
                    'qty' => $r->qty,
                    'total' => $harga,
                    'keterangan' => $r->keterangan,
                    'status' => 'disewa'
                ]);
                // dd($qty);

                if ($qty != 0) {
                    $fc = Fotocopy::where('id_fotocopy',$r->id_fotocopy)->update([
                        'qty' => $qty,
                    ]);
                    // dd('status tidak update');
                }else{
                    // dd($qty);
                    $fc = Fotocopy::where('id_fotocopy',$r->id_fotocopy)->update([
                        'qty' => $qty,
                        'status' => 'tidak'
                    ]);
                }
                
                toastr()->success('Penyewaan baru berhasil ditambahkan!');
                return redirect('admin/penyewaan');
            }
        }else{
            toastr()->error($validator->messages()->first());
            return redirect()->back()->withInput();
        }
    }

    public function index(){
        $penyewaans = Penyewaan::all();
        return view('admin.penyewaan.index', compact('penyewaans'));
    }
}

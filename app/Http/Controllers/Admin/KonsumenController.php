<?php

namespace App\Http\Controllers\Admin;

use Image;
use Validator;
use App\Konsumen;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class KonsumenController extends Controller
{
    public function create(){
        return view('admin.konsumen.create');
    }

    public function store(Request $r){
        $validator = Validator::make($r->all(), [
            'no_ktp' => 'required',
            'foto_ktp' => 'required|image|mimes:jpeg,png,jpg|max:5024',
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'email' => 'required',
            'alamat' => 'required',
            'no_telp' => 'required',
            'status' => 'required'
        ]);
        if (!$validator->fails()) {
            $foto_ktp = $r->file('foto_ktp');
            $filename = time() . '.' . $foto_ktp->getClientOriginalExtension();
            Image::make($foto_ktp)->save(public_path('/images/ktp/'.$filename));
            $konsumen = Konsumen::create([
                'no_ktp' => $r->no_ktp,
                'foto_ktp' => $filename,
                'nama' => $r->nama,
                'tempat_lahir' => $r->tempat_lahir,
                'tanggal_lahir' => $r->tanggal_lahir,
                'email' => $r->email,
                'alamat' => $r->alamat,
                'no_telp' => $r->no_telp,
                'status' => $r->status
            ]);
            toastr()->success('Konsumen baru berhasil ditambahkan!');
            return redirect('admin/konsumen');
        }else{
            toastr()->error($validator->messages()->first());
            return redirect()->back()->withInput();
        }
    }

    public function index(){
        $konsumens = Konsumen::all();
        return view('admin.konsumen.index', compact('konsumens'));
    }

    public function edit(Request $r, $id){
        $konsumen = Konsumen::where('id_konsumen',$id)->first();
        return view('admin.konsumen.edit', compact('konsumen'));
    }

    public function update(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'no_ktp' => 'required',
            'nama' => 'required',
            'email' => 'required',
            'alamat' => 'required',
            'no_telp' => 'required',
            'status' => 'required'
        ]);
        if (!$validator->fails()) {
            if ($r->hasFile('foto_ktp')) {
                $db = Konsumen::where('id_konsumen',$id)->first();
                $foto_ktp = $r->file('foto_ktp');
                $filename = time() . '.' . $foto_ktp->getClientOriginalExtension();
                unlink(public_path('/images/ktp/').$db->foto_ktp);
                Image::make($foto_ktp)->save(public_path('/images/ktp/'.$filename));
                $konsumen = Konsumen::where('id_konsumen',$id)->update([
                    'no_ktp' => $r->no_ktp,
                    'foto_ktp' => $filename,
                    'nama' => $r->nama,
                    'tempat_lahir' => $r->tempat_lahir,
                    'tanggal_lahir' => $r->tanggal_lahir,
                    'email' => $r->email,
                    'alamat' => $r->alamat,
                    'no_telp' => $r->no_telp,
                    'status' => $r->status
                ]);
                toastr()->success('Konsumen berhasil diubah!');
                return redirect('admin/konsumen');
            }else{
                $konsumen = Konsumen::where('id_konsumen',$id)->update([
                    'no_ktp' => $r->no_ktp,
                    'nama' => $r->nama,
                    'tempat_lahir' => $r->tempat_lahir,
                    'tanggal_lahir' => $r->tanggal_lahir,
                    'email' => $r->email,
                    'alamat' => $r->alamat,
                    'no_telp' => $r->no_telp,
                    'status' => $r->status
                ]);
                toastr()->success('Konsumen berhasil diubah!');
                return redirect('admin/konsumen');
            }
        }else{
            toastr()->error($validator->messages()->first());
            return redirect()->back()->withInput();
        }
    }

    public function destroy($id_konsumen){
        $konsumen = Konsumen::where('id_konsumen',$id_konsumen)->first();
        unlink(public_path('/images/ktp/').$konsumen->foto_ktp);
        $konsumen->delete();
        toastr()->success('Konsumen yang dipilih berhasil dihapus!');
        return redirect('admin/konsumen');
    }
}

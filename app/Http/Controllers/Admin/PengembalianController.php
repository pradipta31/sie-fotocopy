<?php

namespace App\Http\Controllers\Admin;

use Validator;
use App\Fotocopy;
use App\Pengembalian;
use App\Penyewaan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PengembalianController extends Controller
{
    public function pengembalian(Request $r, $id){
        $validator = Validator::make($r->all(),[
            'konsumen_id' => 'required',
            'fotocopy_id' => 'required',
            'tanggal_pengembalian' => 'required',
            'keterangan' => 'required'
        ]);

        if ($validator->fails()) {
            toastr()->error($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $fotocopy = Fotocopy::where('id_fotocopy',$r->fotocopy_id)->first();
            $peny = Penyewaan::where('id_penyewaan',$id)->first();
            $qty = $fotocopy->qty + $peny->qty;
            $pengembalian = Pengembalian::create([
                'penyewaan_id' => $r->penyewaan_id,
                'tanggal_pengembalian' => $r->tanggal_pengembalian,
                'keterangan' => $r->keterangan,
                'status' => 'dikembalikan'
            ]);

            $penyewaan = Penyewaan::where('id_penyewaan',$id)->update([
                'status' => 'dikembalikan'
            ]);

            $fc = Fotocopy::where('id_fotocopy',$r->fotocopy_id)->update([
                'qty' => $qty,
                'status' => 'ready'
            ]);

            toastr()->success('Barang berhasil dikembalikan!');
            return redirect('admin/pengembalian');
        }
    }

    public function index(){
        $pengembalians = Pengembalian::all();
        return view('admin.pengembalian.index', compact('pengembalians'));
    }
}

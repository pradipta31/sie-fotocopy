<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Image;

use App\Fotocopy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FotocopyController extends Controller
{
    public function create(){
        return view('admin.fotocopy.create');
    }

    public function store(Request $r){
        $validator = Validator::make($r->all(), [
            'merk' => 'required',
            'tahun' => 'required',
            'harga' => 'required',
            'gambar' => 'required|image|mimes:jpeg,png,jpg|max:5024',
            'qty' => 'required',
            'spesifikasi' => 'required'
        ]);
        if (!$validator->fails()) {
            $gambar = $r->file('gambar');
            $filename = time() . '.' . $gambar->getClientOriginalExtension();
            if ($r->file('gambar')->isValid()) {
                Image::make($gambar)->save(public_path('/images/fotocopy/'.$filename));
                $fotocopy = Fotocopy::create([
                    'merk' => $r->merk,
                    'tahun' => $r->tahun,
                    'harga' => $r->harga,
                    'gambar' => $filename,
                    'spesifikasi' => $r->spesifikasi,
                    'qty' => $r->qty,
                    'status' => 'ready'
                ]);
                toastr()->success('Fotocopy baru berhasil ditambahkan!');
                return redirect('admin/fotocopy');
            }
        }else{
            toastr()->error($validator->messages()->first());
            return redirect()->back()->withInput();
        }
    }

    public function index(){
        $fotocopies = Fotocopy::all();
        return view('admin.fotocopy.index', compact('fotocopies'));
    }

    public function edit(Request $r, $id){
        $fotocopy = Fotocopy::where('id_fotocopy',$id)->first();
        return view('admin.fotocopy.edit', compact('fotocopy'));
    }

    public function update(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'merk' => 'required',
            'tahun' => 'required',
            'harga' => 'required',
            'spesifikasi' => 'required',
            'qty' => 'required',
            'status' => 'required'
        ]);
        if (!$validator->fails()) {
            if ($r->hasFile('gambar')) {
                $db = Fotocopy::where('id_fotocopy',$id)->first();
                $gambar = $r->file('gambar');
                $filename = time() . '.' . $gambar->getClientOriginalExtension();
                unlink(public_path('/images/fotocopy/').$db->gambar);
                Image::make($gambar)->save(public_path('/images/fotocopy/'.$filename));
                $fotocopy = Fotocopy::where('id_fotocopy',$id)->update([
                    'merk' => $r->merk,
                    'tahun' => $r->tahun,
                    'harga' => $r->harga,
                    'gambar' => $filename,
                    'spesifikasi' => $r->spesifikasi,
                    'qty' => $r->qty,
                    'status' => $r->status
                ]);
                toastr()->success('Fotocopy berhasil di ubah!');
                return redirect('admin/fotocopy');
            }else{
                $fotocopy = Fotocopy::where('id_fotocopy',$id)->update([
                    'merk' => $r->merk,
                    'tahun' => $r->tahun,
                    'harga' => $r->harga,
                    'spesifikasi' => $r->spesifikasi,
                    'status' => $r->status
                ]);
                toastr()->success('Fotocopy berhasil di ubah!');
                return redirect('admin/fotocopy');
            }
        }else{
            toastr()->error($validator->messages()->first());
            return redirect()->back()->withInput();
        }
    }

    public function destroy($id_fotocopy){
        $fotocopy = Fotocopy::where('id_fotocopy',$id_fotocopy)->delete();
        toastr()->success('Fotocopy yang dipilih berhasil dihapus!');
        return redirect('admin/fotocopy');
    }
}

@extends('layouts.master',['activeMenu' => 'penyewaan'])
@section('title','Daftar Penyewaan')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
<section class="content-header">
    <h1>
        Penyewaan
        <small>Daftar Penyewaan</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Daftar Penyewaan</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <a href="{{url('pimpinan/penyewaan/download')}}" class="btn btn-md btn-success" style="margin-bottom: 10px">
                        <i class="fa fa-file-excel-o"></i>
                        Export Excel
                    </a>
                    <div class="table-responsive">
                        <table id="tabelSewa" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Konsumen</th>
                                    <th>Fotocopy</th>
                                    <th>Tanggal Transaksi</th>
                                    <th>Bukti Pembayaran</th>
                                    <th>Keterangan</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($penyewaans as $rent)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$rent->konsumen->nama}}</td>
                                        <td>{{$rent->fotocopy->merk}}</td>
                                        <td>
                                            {{date('d-m-Y', strtotime($rent->tanggal_transaksi))}}
                                        </td>
                                        <td>
                                            <a href="#">
                                                <img src="{{asset('images/bukti/'.$rent->bukti_pembayaran)}}" onClick="showImage('{{$rent->bukti_pembayaran}}');" class="img-responsive" width="75px" height="75px">
                                            </a>
                                        </td>
                                        <td>{{$rent->keterangan}}</td>
                                        <td>
                                            @if ($rent->status == 'disewa')
                                                <span class="label label-success">Sedang Disewa</span>
                                            @else
                                                <span class="label label-warning">Sudah Dikembalikan</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tabelSewa').dataTable()
        });

        function showImage(bukti_transaksi){
            bootbox.dialog({
                message: '<img src="{{asset('images/bukti')}}/'+bukti_transaksi+'" class="img-responsive">',
                closeButton: true,
                size: 'medium'
            });
        }

        function pengembalianModal(){
            $('#pengembalianFc').modal('show');
        }
    </script>
@endsection

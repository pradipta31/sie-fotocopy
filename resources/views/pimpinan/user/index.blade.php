@extends('layouts.master',['activeMenu' => 'users'])
@section('title','Daftar User')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
<section class="content-header">
    <h1>
        User
        <small>Daftar User</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Daftar User</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <a href="{{url('pimpinan/user/tambah')}}" class="btn btn-primary btn-md" style="margin-bottom: 5px">
                        <i class="fa fa-plus"></i>
                        Tambah User Baru
                    </a>
                    <!-- BOOK CREATE MODAL -->
                    <div class="table-responsive">
                        <table id="tabelUser" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Opsi</th>
                                    <th>Nama</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Level</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($users as $user)
                                    <tr>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
                                                    <span class="">Option <i class="caret"></i></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="javascript:void(0);" onclick="updatePassword('{{$user->id_user}}')">Change Password</a></li>

                                                    <li class="divider"></li>
                                                    @if ($user->status == 1)
                                                        <li><a href="javascript:void(0);" onclick="nonaktifStatus('{{$user->id_user}}')">Non Aktifkan User</a></li>
                                                    @else
                                                        <li><a href="javascript:void(0);" onclick="aktifStatus('{{$user->id_user}}')">Aktifkan User</a></li>
                                                    @endif
                                                    
                                                </ul>
                                            </div>
                                        </td>
                                        <td>{{$user->nama}}</td>
                                        <td>{{$user->username}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>
                                            @if ($user->level == 1)
                                                <span class="label label-info">Owner</span>
                                            @elseif ($user->level == 2)
                                                <span class="label label-info">Admin</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($user->status == 1)
                                                <span class="label label-success">Aktif</span>
                                            @else
                                                <span class="label label-warning">Non Aktif</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<form class="hidden" action="" method="post" id="formDelete">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="delete">
</form>
<form class="hidden" action="" method="post" id="formActive">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="put">
</form>
<form class="hidden" action="" method="post" id="formInnactive">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="put">
</form>
<form class="hidden" action="" method="post" id="formPassword">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="put">
    <input type="hidden" name="password" value="" id="password">
</form>

@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        $('#level').on('change', function(){
            var val = this.value;
            $('#nis').hide();
            if (val == 3) {
                console.log('nis');
                $('#nis').show();
            }else{
                $('#nis').hide();
            }
        });
        function randomPassword(length) {
            var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP1234567890";
            var pass = "";
            for (var x = 0; x < length; x++) {
                var i = Math.floor(Math.random() * chars.length);
                pass += chars.charAt(i);
            }
            return pass;
        }

        function generate() {
            formUser.password.value = randomPassword(formUser.length.value);
        }
        function deleteUser(id){
            swal({
                title: "Anda yakin?",
                text: "User akan terhapus secara permanen!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Berhasil! User yang anda pilih berhasil terhapus!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formDelete').attr('action', '{{url('pimpinan/user')}}/'+id);
                        $('#formDelete').submit();
                    }); 
                }
            });
        }
        function nonaktifStatus(id){
            swal({
                title: "Anda yakin?",
                text: "User akan dinonaktifkan, user dapat kembali diaktifkan!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willInnactive) => {
                if (willInnactive) {
                    swal("Berhasil! User yang anda pilih berhasil dinonaktifkan!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formInnactive').attr('action', '{{url('pimpinan/user/nonaktif')}}/'+id);
                        $('#formInnactive').submit();
                    }); 
                }
            });
        }
        function aktifStatus(id){
            swal({
                title: "Anda yakin?",
                text: "User akan diaktifkan!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willActive) => {
                if (willActive) {
                    swal("Berhasil! User yang anda pilih berhasil diaktifkan!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formActive').attr('action', '{{url('pimpinan/user/aktif')}}/'+id);
                        $('#formActive').submit();
                    }); 
                }
            });
        }
        function updatePassword(id){
            bootbox.prompt({
                title: 'Masukan password.',
                inputType: 'password',
                size: 'small',
                callback: function(result){
                    if (result != null ) {
                        bootbox.prompt({
                            title: 'Masukan kembali password anda.',
                            inputType: 'password',
                            size: 'small',
                            callback: function(password){
                                if (password != null) {
                                    if (result == password) {
                                        $('#password').val(password);
                                        $('#formPassword').attr('action', '{{url('pimpinan/user/password')}}/'+id);
                                        $('#formPassword').submit();
                                    }else {
                                        bootbox.alert("Password tidak sama. Silakan ulangi !");
                                    }
                                }
                            }
                        });
                    }
                }
            });
        }
        $(function(){
            $('#tabelUser').dataTable()
        });
    </script>
@endsection

@extends('layouts.master',['activeMenu' => 'users'])
@section('title','Tambah User Baru')

@section('content')
  <section class="content-header">
    <h1>
      User
      <small>Tambah User Baru</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li class="active">Tambah User Baru</li>
    </ol>
  </section>
  <section class="content">
    <form class="" action="{{url('pimpinan/user/tambah')}}" method="post" name="formUser" enctype="multipart/form-data">
      {{csrf_field()}}
      <input type="hidden" name="length" value="6">
      <div class="row">
        <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah User Baru</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <label for="">Nama</label>
                    <input type="text" class="form-control" name="nama" value="{{old('nama')}}" placeholder="Masukan nama Admin">
                </div>
                <div class="form-group">
                  <label for="">Username</label>
                  <input type="text" class="form-control" name="username" value="{{old('username')}}" placeholder="Masukan username">
                </div>
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="text" class="form-control" name="email" value="{{old('email')}}" placeholder="Ex: yourmail@mail.com">
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-9">
                            <label class="col-form-label">Password</label>
                            <input type="text" class="form-control" name="password">
                        </div>
                        <div class="col-md-3">
                            <label class="col-form-label"></label>
                            <button type="button" class="btn btn-success btn-md" onclick="generate();" style="margin-top: 20%">
                                <i class="fa fa-refresh"></i>
                                Generate
                            </button>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Nomor Telepon</label>
                    <input type="text" class="form-control" name="no_telp" value="{{old('no_telp')}}" placeholder="Masukan Nomor Telepon">
                </div>
                <div class="form-group">
                    <label for="">Pilih Akses User</label>
                    <select name="level" class="form-control" value="{{old('level')}}">
                        <option value="1">Owner</option>
                        <option value="2">Admin</option>
                    </select>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </section>
@endsection
@section('js')
    <script type="text/javascript">
        function randomPassword(length) {
            var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP1234567890";
            var pass = "";
            for (var x = 0; x < length; x++) {
                var i = Math.floor(Math.random() * chars.length);
                pass += chars.charAt(i);
            }
            return pass;
        }

        function generate() {
            formUser.password.value = randomPassword(formUser.length.value);
        }
        function saveThis(r){
          swal({
            title: "User baru akan ditambahkan",
            text: "User akan menerima email dan harus melakukan konfirmasi untuk dapat melakukan login.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((result) => {
            if (result) {
              swal("Email berhasil terkirim!", {
                icon: "success",
              });
            }
          });
        }
    </script>
@endsection

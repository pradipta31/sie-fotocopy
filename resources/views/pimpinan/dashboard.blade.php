@extends('layouts.master',['activeMenu' => 'dashboard'])
@section('title','Dashboard')
@section('css')
    
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    <section class="content">
        @php
            if (Auth::user()->level == 1) {
                $user = 'pimpinan';
            }else{
                $user = 'admin';
            }
        @endphp
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-md-12">
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Selamat datang {{Auth::user()->nama}}</h4>
                    Pada halaman dashboard anda dapat melihat beberapa informasi mengenai website anda.
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{$konsumens}}</h3>
                        <p>Total Konsumen</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div>
                    <a href="{{url($user.'/konsumen')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{$fotocopies}}</h3>
        
                        <p>Total Fotocopy</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-gears"></i>
                    </div>
                    <a href="{{url($user.'/fotocopy')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{$penyewaans}}</h3>
        
                        <p>Total Penyewaan</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-share"></i>
                    </div>
                    <a href="{{url($user.'/penyewaan')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{$pengembalians}}</h3>
        
                        <p>Total Pengembalian</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-undo"></i>
                    </div>
                    <a href="{{url($user.'/pengembalian')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <!-- BAR CHART -->
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Chart Transaksi Penyewaan & Pengembalian {{ Carbon\Carbon::now()->year }}</h3>
                </div>
                <div class="box-body">
                    <div class="chart">
                        <canvas id="dashboardTransaksiChart" style="height:230px"></canvas>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
            </div>
        </div>
    </section>
@endsection

@section('js')
    <!-- ChartJS -->
    <script src="{{ asset('backend/bower_components/chart.js/Chart.js') }}"></script>
    <script>
        var dataChart = @json($dataChart);
        $(function () {
            var chartData = {
                labels  : dataChart.months,
                datasets: [
                    {
                        label               : Object.keys(dataChart)[1].toUpperCase(),
                        fillColor           : 'rgba(210, 214, 222, 1)',
                        strokeColor         : 'rgba(210, 214, 222, 1)',
                        pointColor          : 'rgba(210, 214, 222, 1)',
                        pointStrokeColor    : '#c1c7d1',
                        pointHighlightFill  : '#fff',
                        pointHighlightStroke: 'rgba(220,220,220,1)',
                        data                : Object.values(dataChart.penyewaan)
                    },
                    {
                        label               : Object.keys(dataChart)[2].toUpperCase(),
                        fillColor           : 'rgba(60,141,188,0.9)',
                        strokeColor         : 'rgba(60,141,188,0.8)',
                        pointColor          : '#3b8bba',
                        pointStrokeColor    : 'rgba(60,141,188,1)',
                        pointHighlightFill  : '#fff',
                        pointHighlightStroke: 'rgba(60,141,188,1)',
                        data                : Object.values(dataChart.pengembalian)
                    }
                ]
          }
          
          //-------------
          //- BAR CHART -
          //-------------
          var barChartCanvas                   = $('#dashboardTransaksiChart').get(0).getContext('2d')
          var barChart                         = new Chart(barChartCanvas)
          var barChartData                     = chartData
          barChartData.datasets[1].fillColor   = '#00a65a'
          barChartData.datasets[1].strokeColor = '#00a65a'
          barChartData.datasets[1].pointColor  = '#00a65a'
          var barChartOptions                  = {
            //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
            scaleBeginAtZero        : true,
            //Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines      : true,
            //String - Colour of the grid lines
            scaleGridLineColor      : 'rgba(0,0,0,.05)',
            //Number - Width of the grid lines
            scaleGridLineWidth      : 1,
            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines  : true,
            //Boolean - If there is a stroke on each bar
            barShowStroke           : true,
            //Number - Pixel width of the bar stroke
            barStrokeWidth          : 2,
            //Number - Spacing between each of the X value sets
            barValueSpacing         : 5,
            //Number - Spacing between data sets within X values
            barDatasetSpacing       : 1,
            //String - A legend template
            legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
            //Boolean - whether to make the chart responsive
            responsive              : true,
            maintainAspectRatio     : true,
            multiTooltipTemplate: "<%= datasetLabel %>: <%= value %>",
          }
      
          barChartOptions.datasetFill = false
          barChart.Bar(barChartData, barChartOptions)
        })
      </script>
@endsection

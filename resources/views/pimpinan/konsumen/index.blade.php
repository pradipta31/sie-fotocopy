@extends('layouts.master',['activeMenu' => 'konsumen'])
@section('title','Daftar Konsumen')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
<section class="content-header">
    <h1>
        Konsumen
        <small>Daftar Konsumen</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Daftar Konsumen</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <a href="{{url('pimpinan/konsumen/download')}}" class="btn btn-md btn-success" style="margin-bottom: 10px">
                        <i class="fa fa-file-excel-o"></i>
                        Export Excel
                    </a>
                    <div class="table-responsive">
                        <table id="tabelKonsumen" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>No KTP</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Alamat</th>
                                    <th>No HP</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($konsumens as $konsumen)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$konsumen->no_ktp}}</td>
                                        <td>{{$konsumen->nama}}</td>
                                        <td>{{$konsumen->email}}</td>
                                        <td>
                                            {{$konsumen->alamat}}
                                        </td>
                                        <td>{{$konsumen->no_telp}}</td>
                                        <td>
                                            @if ($konsumen->status == 1)
                                                <span class="label label-success">Aktif</span>
                                            @else
                                                <span class="label label-warning">Tidak Aktif</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tabelKonsumen').dataTable()
        });
    </script>
@endsection

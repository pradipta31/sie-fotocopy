@extends('layouts.master',['activeMenu' => 'fotocopy'])
@section('title','Daftar Fotocopy')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
<section class="content-header">
    <h1>
        Fotocopy
        <small>Daftar Fotocopy</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Daftar Fotocopy</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <a href="{{url('pimpinan/fotocopy/download')}}" class="btn btn-md btn-success" style="margin-bottom: 10px">
                        <i class="fa fa-file-excel-o"></i>
                        Export Excel
                    </a>
                    <div class="table-responsive">
                        <table id="tabelFotocopy" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Merk</th>
                                    <th>Tahun</th>
                                    <th>Harga</th>
                                    <th>Gambar</th>
                                    <th>Spesifikasi</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($fotocopies as $fotocopy)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$fotocopy->merk}}</td>
                                        <td>{{$fotocopy->tahun}}</td>
                                        <td>Rp. {{$fotocopy->harga}}</td>
                                        <td>
                                            <a href="#">
                                                <img src="{{asset('images/fotocopy/'.$fotocopy->gambar)}}" onClick="showImage('{{$fotocopy->gambar}}');" class="img-responsive" width="75px" height="75px">
                                            </a>
                                        </td>
                                        <td>{{$fotocopy->spesifikasi}}</td>
                                        <td>
                                            @if ($fotocopy->status == 'ada')
                                                <span class="label label-success">Ada</span>
                                            @else
                                                <span class="label label-warning">Tidak Ada</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tabelFotocopy').dataTable()
        });

        function showImage(gambar){
            bootbox.dialog({
                message: '<img src="{{asset('images/fotocopy')}}/'+gambar+'" class="img-responsive">',
                closeButton: true,
                size: 'medium'
            });
        }
    </script>
@endsection

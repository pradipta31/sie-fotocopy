<header class="main-header">
    <a href="{{url('home')}}" class="logo">
        
        <span class="logo-mini"><b>SIE</b></span>
        <span class="logo-lg">
            <img src="{{asset('images/logo/logo-angkasa-2.png')}}" alt="" style="height: 30px; width: 30px">
            <b>SIE</b> FOTOCOPY</span>
    </a>
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{asset('images/avatar5.png')}}" class="user-image" alt="User Image">
                        <span class="hidden-xs">{{Auth::user()->username}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header" style="background-image: url({{asset('images/logo/logo-angkasa-1.png')}}); background-repeat:no-repeat; background-position: center;">
                            <img src="{{asset('images/avatar5.png')}}" class="img-circle" alt="User Image">
                            <p>
                                {{Auth::user()->username}}
                                <small>
                                    @if (Auth::user()->level == 1)
                                    Owner
                                    @elseif (Auth::user()->level == 2)
                                    Admin
                                    @endif
                                </small>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{url('profile')}}" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-default btn-flat">Sign out</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
  
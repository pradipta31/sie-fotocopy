<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('images/avatar5.png')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{str_limit(Auth::user()->nama,24)}}</p>
                <a><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">DASHBOARD NAVIGATION</li>
            <li class="{{$activeMenu == 'dashboard' ? 'active' : ''}}"><a href="{{url('home')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            @if (Auth::user()->level == 1)
                <li class="header">MASTER NAVIGATION</li>
                <li class="treeview {{$activeMenu == 'users' ? 'active' : ''}}">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span>User</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{url('pimpinan/user/tambah')}}"><i class="fa fa-plus"></i> Tambah User Baru</a></li>
                        <li><a href="{{url('pimpinan/user')}}"><i class="fa fa-circle-o"></i> User</a></li>
                    </ul>
                </li>

                <li class="{{$activeMenu == 'konsumen' ? 'active' : ''}}"><a href="{{url('pimpinan/konsumen')}}"><i class="fa fa-users"></i> <span>Data Konsumen</span></a></li>
                <li class="{{$activeMenu == 'fotocopy' ? 'active' : ''}}"><a href="{{url('pimpinan/fotocopy')}}"><i class="fa fa-gears"></i> <span>Data Fotocopy</span></a></li>
                <li class="{{$activeMenu == 'penyewaan' ? 'active' : ''}}"><a href="{{url('pimpinan/penyewaan')}}"><i class="fa fa-share"></i> <span>Data Penyewaan</span></a></li>
                <li class="{{$activeMenu == 'pengembalian' ? 'active' : ''}}"><a href="{{url('pimpinan/pengembalian')}}"><i class="fa fa-undo"></i> <span>Data Pengembalian</span></a></li>
            @else
                <li class="header">MASTER NAVIGATION</li>
                <li class="treeview {{$activeMenu == 'konsumen' ? 'active' : ''}}">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span>Konsumen</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{url('admin/konsumen/tambah')}}"><i class="fa fa-plus"></i> Tambah Konsumen Baru</a></li>
                        <li><a href="{{url('admin/konsumen')}}"><i class="fa fa-circle-o"></i> Daftar Konsumen</a></li>
                    </ul>
                </li>

                <li class="treeview {{$activeMenu == 'fotocopy' ? 'active' : ''}}">
                    <a href="#">
                        <i class="fa fa-gears"></i>
                        <span>Fotocopy</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{url('admin/fotocopy/tambah')}}"><i class="fa fa-plus"></i> Tambah Fotocopy Baru</a></li>
                        <li><a href="{{url('admin/fotocopy')}}"><i class="fa fa-circle-o"></i> Data Fotocopy</a></li>
                    </ul>
                </li>

                <li class="treeview {{$activeMenu == 'penyewaan' ? 'active' : ''}}">
                    <a href="#">
                        <i class="fa fa-share"></i>
                        <span>Penyewaan</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{url('admin/penyewaan/tambah')}}"><i class="fa fa-plus"></i> Buat Penyewaan Baru</a></li>
                        <li><a href="{{url('admin/penyewaan')}}"><i class="fa fa-circle-o"></i> Daftar Sewa</a></li>
                    </ul>
                </li>

                <li class="{{$activeMenu == 'pengembalian' ? 'active' : ''}}"><a href="{{url('admin/pengembalian')}}"><i class="fa fa-undo"></i> <span>Data Pengembalian</span></a></li>
            @endif
        </ul>
    </section>
</aside>
  
@extends('layouts.master',['activeMenu' => 'penyewaan'])
@section('title','Daftar Penyewaan')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
<section class="content-header">
    <h1>
        Penyewaan
        <small>Daftar Penyewaan</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Daftar Penyewaan</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <a href="{{url('admin/penyewaan/tambah')}}" class="btn btn-primary btn-md" style="margin-bottom: 5px">
                        <i class="fa fa-plus"></i>
                        Tambah Penyewaan Baru
                    </a>
                    <!-- BOOK CREATE MODAL -->
                    <div class="table-responsive">
                        <table id="tabelSewa" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Konsumen</th>
                                    <th>Fotocopy</th>
                                    <th>Tanggal Penyewaan</th>
                                    <th>Bukti Pembayaran</th>
                                    <th>Jumlah Sewa</th>
                                    <th>Total</th>
                                    <th>Keterangan</th>
                                    <th>Status</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($penyewaans as $rent)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$rent->konsumen->nama}}</td>
                                        <td>{{$rent->fotocopy->merk}}</td>
                                        <td>
                                            {{date('d-m-Y', strtotime($rent->tanggal_transaksi))}}
                                        </td>
                                        <td>
                                            <a href="#">
                                                <img src="{{asset('images/bukti/'.$rent->bukti_pembayaran)}}" onClick="showImage('{{$rent->bukti_pembayaran}}');" class="img-responsive" width="75px" height="75px">
                                            </a>
                                        </td>
                                        <td>{{$rent->qty}}</td>
                                        <td>Rp. {{$rent->total}}</td>
                                        <td>{{$rent->keterangan}}</td>
                                        <td>
                                            @if ($rent->status == 'disewa')
                                                <span class="label label-success">Sedang Disewa</span>
                                            @else
                                                <span class="label label-warning">Sudah Dikembalikan</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($rent->status == 'disewa')
                                                <a href="javascript:void(0);" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#pengembalianFc{{$rent->id_penyewaan}}">
                                                    <i class="fa fa-undo"></i>
                                                    Pengembalian
                                                </a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                    </tr>

                                    <div class="modal fade" id="pengembalianFc{{$rent->id_penyewaan}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h3 class="modal-title" id="exampleModalLabel">Pengembalian Fotocopy {{$rent->fotocopy->merk}}</h3>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form action="{{url('admin/penyewaan/'.$rent->id_penyewaan.'/pengembalian')}}" method="post" name="formUser" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="penyewaan_id" value="{{$rent->id_penyewaan}}">
                                                    <input type="hidden" name="fotocopy_id" value="{{$rent->fotocopy_id}}">
                                                    <input type="hidden" name="konsumen_id" value="{{$rent->konsumen_id}}">
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label class="col-form-label">Konsumen</label>
                                                                    <input type="text" name="konsumen_name" class="form-control" value="{{$rent->konsumen->nama}}" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label class="col-form-label">Fotocopy</label>
                                                                    <input type="text" name="fotocopy_name" class="form-control" value="{{$rent->fotocopy->merk}}" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label class="col-form-label">Tanggal Pengembalian</label>
                                                                    <input type="date" class="form-control" name="tanggal_pengembalian" value="{{old('tanggal_pengembalian')}}" style="height:25%">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label class="col-form-label">Keterangan Pengembalian</label>
                                                                    <textarea name="keterangan" class="form-control" cols="30" rows="5">{{old('keterangan')}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                                        <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tabelSewa').dataTable()
        });

        function showImage(bukti_transaksi){
            bootbox.dialog({
                message: '<img src="{{asset('images/bukti')}}/'+bukti_transaksi+'" class="img-responsive">',
                closeButton: true,
                size: 'medium'
            });
        }

        function pengembalianModal(){
            $('#pengembalianFc').modal('show');
        }
    </script>
@endsection

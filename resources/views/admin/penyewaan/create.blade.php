@extends('layouts.master',['activeMenu' => 'penyewaan'])
@section('title','Tambah Penyewaan Baru')
@section('css')
    <link href="{{asset('backend/plugins/select2-4.0.13/dist/css/select2.min.css')}}" rel="stylesheet" />
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Penyewaan
            <small>Tambah Penyewaan Baru</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Tambah Penyewaan Baru</li>
        </ol>
    </section>
    <section class="content">
        <form class="" action="{{url('admin/penyewaan/tambah')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tambah Penyewaan Baru</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Konsumen Penyewa</label>
                                <select name="id_konsumen" id="konsumen_id" class="js-example-placeholder-single js-states form-control" value="{{old('id_konsumen')}}">
                                    
                                    @foreach ($konsumens as $konsumen)
                                        <option value="{{$konsumen->id_konsumen}}">{{$konsumen->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Fotocopy</label>
                                <select name="id_fotocopy" id="fotocopy_id" class="form-control" value="{{old('id_fotocopy')}}">
                                    @foreach ($fotocopies as $fotocopy)
                                        <option value="{{$fotocopy->id_fotocopy}}">{{$fotocopy->merk}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Tanggal Penyewaan</label>
                                <input type="date" class="form-control" name="tanggal_transaksi" value="{{old('tanggal_transaksi')}}" style="height:25%">
                            </div>
                            <div class="form-group">
                                <label for="">Jumlah yang Disewa</label>
                                <input type="number" name="qty" class="form-control" value="{{old('qty')}}">
                            </div>
                            <div class="form-group">
                                <label for="">Bukti Pembayaran</label>
                                <input type="file" name="bukti_pembayaran" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Keterangan</label>
                                <textarea name="keterangan" id="" class="form-control" cols="30" rows="5">{{old('keterangan')}}</textarea>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tabel Fotocopy</h3>
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="tabelFotocopy" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Merk</th>
                                            <th>Tahun</th>
                                            <th>Harga</th>
                                            <th>Jumlah Tersedia</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach($fotocopies as $fotocopy)
                                            <tr>
                                                <td>{{$no++}}</td>
                                                <td>{{$fotocopy->merk}}</td>
                                                <td>{{$fotocopy->tahun}}</td>
                                                <td>Rp. {{$fotocopy->harga}}</td>
                                                <td>{{$fotocopy->qty}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
@section('js')
    <script src="{{asset('backend/plugins/select2-4.0.13/dist/js/select2.min.js')}}"></script>
    <script>
        $(document).ready(function(){
            var sel = $('#konsumen_id').select2({
                placeholder: "Pilih konsumen",
                allowClear: true
            });
            sel.data('select2').$selection.css('padding', '.5rem .55rem');
            sel.data('select2').$selection.css('height', '34px');

            var fot = $('#fotocopy_id').select2({
                placeholder: "Pilih Fotocopy",
                allowClear: true
            });
            fot.data('select2').$selection.css('padding', '.5rem .55rem');
            fot.data('select2').$selection.css('height', '34px');

        });

        $(function(){
            $('#tabelFotocopy').dataTable()
        });
    </script>
@endsection


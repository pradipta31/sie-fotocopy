@extends('layouts.master',['activeMenu' => 'fotocopy'])
@section('title','Edit Fotocopy '.$fotocopy->merk)

@section('content')
    <section class="content-header">
        <h1>
            Fotocopy
            <small>Edit Fotocopy {{$fotocopy->merk}}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Edit Fotocopy {{$fotocopy->merk}}</li>
        </ol>
    </section>
    <section class="content">
        <form class="" action="{{url('admin/fotocopy/'.$fotocopy->id_fotocopy.'/edit')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="put">
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Fotocopy {{$fotocopy->merk}}</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Merk</label>
                                <input type="text" class="form-control" name="merk" value="{{$fotocopy->merk}}" placeholder="Masukan Merk Fotocopy">
                            </div>
                            <div class="form-group">
                                <label for="">Tahun</label>
                                <input type="text" class="form-control" name="tahun" value="{{$fotocopy->tahun}}" placeholder="Masukan tahun">
                            </div>
                            <div class="form-group">
                                <label for="">Harga</label>
                                <div class="input-group">
                                    <span class="input-group-addon">Rp.</span>
                                    <input type="text" name="harga" value="{{$fotocopy->harga}}" class="form-control" placeholder="Masukkan harga" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Qty</label>
                                <input type="number" name="qty" class="form-control" value="{{$fotocopy->qty}}">
                            </div>
                            <div class="form-group">
                                <label for="">Gambar</label>
                                <input type="file" name="gambar" class="form-control">
                                <small>Nb: Kosongkan jika tidak ingin mengubah gambar.</small>
                            </div>
                            <div class="form-group">
                                <label for="">Spesifikasi</label>
                                <input type="text" name="spesifikasi" value="{{$fotocopy->spesifikasi}}" class="form-control" placeholder="Masukan spesifikasi">
                            </div>
                            <div class="form-group">
                                <label for="">Status</label>
                                <select name="status" class="form-control" value="{{$fotocopy->status}}">
                                    <option value="ready" {{$fotocopy->status == 'ready' ? 'selected' : ''}}>Ready</option>
                                    <option value="tidak" {{$fotocopy->status == 'tidak' ? 'selected' : ''}}>Sedang Disewa</option>
                                </select>
                            </div>
                            <div class="box-footer">
                                <a href="{{url('admin/fotocopy')}}" class="btn btn-default">Kembali</a>
                                <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection

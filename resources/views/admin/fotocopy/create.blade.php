@extends('layouts.master',['activeMenu' => 'fotocopy'])
@section('title','Tambah Fotocopy Baru')

@section('content')
    <section class="content-header">
        <h1>
            Fotocopy
            <small>Tambah Fotocopy Baru</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Tambah Fotocopy Baru</li>
        </ol>
    </section>
    <section class="content">
        <form class="" action="{{url('admin/fotocopy/tambah')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tambah Fotocopy Baru</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Merk</label>
                                <input type="text" class="form-control" name="merk" value="{{old('merk')}}" placeholder="Masukan Merk Fotocopy">
                            </div>
                            <div class="form-group">
                                <label for="">Tahun</label>
                                <input type="text" class="form-control" name="tahun" value="{{old('tahun')}}" placeholder="Masukan tahun">
                            </div>
                            <div class="form-group">
                                <label for="">Harga Sewa</label>
                                <div class="input-group">
                                    <span class="input-group-addon">Rp.</span>
                                    <input type="text" name="harga" value="{{old('harga')}}" class="form-control" placeholder="Masukkan harga" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Qty</label>
                                <input type="number" name="qty" class="form-control" value="{{old('qty')}}">
                            </div>
                            <div class="form-group">
                                <label for="">Gambar</label>
                                <input type="file" name="gambar" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Spesifikasi</label>
                                <textarea name="spesifikasi" class="form-control" id="" cols="30" rows="3">{{old('spesifikasi')}}</textarea>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection


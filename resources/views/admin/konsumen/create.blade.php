@extends('layouts.master',['activeMenu' => 'konsumen'])
@section('title','Tambah Konsumen Baru')

@section('content')
    <section class="content-header">
        <h1>
            Konsumen
            <small>Tambah Konsumen Baru</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Tambah Konsumen Baru</li>
        </ol>
    </section>
    <section class="content">
        <form class="" action="{{url('admin/konsumen/tambah')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tambah Konsumen Baru</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">No KTP</label>
                                <input type="text" class="form-control" name="no_ktp" value="{{old('no_ktp')}}" placeholder="Masukan No KTP Konsumen">
                            </div>
                            <div class="form-group">
                                <label for="">Upload Foto KTP</label>
                                <input type="file" name="foto_ktp" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Nama</label>
                                <input type="text" class="form-control" name="nama" value="{{old('nama')}}" placeholder="Masukan Nama">
                            </div>
                            <div class="form-group">
                                <div class="col-md-6" style="margin-left: -15px">
                                    <label for="">Tempat Lahir</label>
                                    <input type="text" class="form-control" name="tempat_lahir" value="{{old('tempat_lahir')}}" placeholder="Masukan Tempat Lahir">
                                </div>
                                <div class="col-md-6" style="margin-right: -15px">
                                    <label for="">Tanggal Lahir</label>
                                    <input type="date" class="form-control" name="tanggal_lahir" value="{{old('tanggal_lahir')}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="text" class="form-control" name="email" value="{{old('email')}}" placeholder="Masukan Email">
                            </div>
                            <div class="form-group">
                                <label for="">Alamat</label>
                                <input type="text" class="form-control" name="alamat" value="{{old('alamat')}}" placeholder="Masukan Alamat">
                            </div>
                            <div class="form-group">
                                <label for="">Nomor Telepon</label>
                                <input type="text" name="no_telp" value="{{old('no_telp')}}" class="form-control" placeholder="Masukan Nomor Telepon">
                            </div>
                            
                            <div class="form-group">
                                <label for="">Status</label>
                                <select name="status" class="form-control" value="{{old('status')}}">
                                    <option value="1">Aktif</option>
                                    <option value="0">Non Aktif</option>
                                </select>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection


@extends('layouts.master',['activeMenu' => 'konsumen'])
@section('title','Daftar Konsumen')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
<section class="content-header">
    <h1>
        Konsumen
        <small>Daftar Konsumen</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Daftar Konsumen</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <a href="{{url('admin/konsumen/tambah')}}" class="btn btn-primary btn-md" style="margin-bottom: 5px">
                        <i class="fa fa-plus"></i>
                        Tambah Konsumen Baru
                    </a>
                    <!-- BOOK CREATE MODAL -->
                    <div class="table-responsive">
                        <table id="tabelKonsumen" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>No KTP</th>
                                    <th>Foto KTP</th>
                                    <th>Nama</th>
                                    <th>TTL</th>
                                    <th>Email</th>
                                    <th>Alamat</th>
                                    <th>No HP</th>
                                    <th>Status</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($konsumens as $konsumen)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$konsumen->no_ktp}}</td>
                                        <td>
                                            <a href="#">
                                                <img src="{{asset('images/ktp/'.$konsumen->foto_ktp)}}" onClick="showImage('{{$konsumen->foto_ktp}}');" class="img-responsive" width="75px" height="75px">
                                            </a>
                                        </td>
                                        <td>{{$konsumen->nama}}</td>
                                        <td>
                                            {{$konsumen->tempat_lahir}}, {{date('d-m-Y',strtotime($konsumen->tanggal_lahir))}}
                                        </td>
                                        <td>{{$konsumen->email}}</td>
                                        <td>
                                            {{$konsumen->alamat}}
                                        </td>
                                        <td>{{$konsumen->no_telp}}</td>
                                        <td>
                                            @if ($konsumen->status == 1)
                                                <span class="label label-success">Aktif</span>
                                            @else
                                                <span class="label label-warning">Tidak Aktif</span>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{url('admin/konsumen/'.$konsumen->id_konsumen.'/edit')}}" class="btn btn-sm btn-warning">
                                                <i class="fa fa-pencil"></i>
                                                {{-- Edit --}}
                                            </a>
                                            <a href="javascript:void(0);" class="btn btn-sm btn-danger" onclick="deleteKonsumen('{{$konsumen->id_konsumen}}')">
                                                <i class="fa fa-trash"></i>
                                                {{-- Hapus --}}
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<form class="hidden" action="" method="post" id="formDelete">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="delete">
</form>
@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tabelKonsumen').dataTable()
        });

        function showImage(foto_ktp){
            bootbox.dialog({
                message: '<img src="{{asset('images/ktp')}}/'+foto_ktp+'" class="img-responsive">',
                closeButton: true,
                size: 'medium'
            });
        }

        function deleteKonsumen(id_konsumen){
            bootbox.confirm({
                title: "Hapus konsumen ini?",
                message: "Menghapus konsumen dapat menghilangkan data-data konsumen tersebut secara permanen dan tidak dapat dikembalikan.",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> Cancel'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Confirm'
                    }
                },
                callback: function (result) {
                    if (result) {
                        $('#formDelete').attr('action', '{{url('admin/konsumen/delete')}}/'+id_konsumen);
                        $('#formDelete').submit();
                    }
                }
            });
        }
    </script>
@endsection

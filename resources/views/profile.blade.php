@extends('layouts.master',['activeMenu' => ''])
@section('title','Profile')

@section('content')
  <section class="content-header">
    <h1>
      Profile
      <small>User Profile</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li class="active">User Profile</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-8">

        <!-- Profile Image -->
        <div class="box box-primary">
          <div class="box-body box-profile">
            <form class="form-horizontal" action="{{url('profile/'.$user->id_user)}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="put">
                <div class="form-group">
                  <label for="inputName" class="col-sm-2 control-label">Nama Lengkap</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{$user->nama}}" name="nama">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputName" class="col-sm-2 control-label">Username</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{$user->username}}" name="username">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputName" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-10">
                    <input type="email" class="form-control" value="{{$user->email}}" name="email">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputName" class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" name="password" id="password" placeholder="***">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputName" class="col-sm-2 control-label">Re-Type Password</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" name="confirmation_password" id="confirmation_password" placeholder="***">
                    <span id="message"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputName" class="col-sm-2 control-label">Nomor Telepon</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="no_telp" value="{{$user->no_telp}}">
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Simpan</button>
                  </div>
                </div>
              </form>
          </div>
        </div>
      </div>
      {{-- <div class="col-md-9">
        <div class="nav-tabs-custom">
          <div class="tab-content">
            <div class="active tab-pane" id="activity">

            <div class="tab-pane">
              <form class="form-horizontal" action="{{url('profile/bio/'.$user->id)}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="put">
                <div class="form-group">
                  <label for="inputName" class="col-sm-2 control-label">NIA</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{$user->nia}}" name="nia">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputName" class="col-sm-2 control-label">Nama Lengkap</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{$user->nama_lengkap}}" name="nama_lengkap">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputName" class="col-sm-2 control-label">Username</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{$user->username}}" name="username">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputName" class="col-sm-2 control-label">Nama Panggilan</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{$user->nama_panggilan}}" name="nama_panggilan">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputName" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-10">
                    <input type="email" class="form-control" value="{{$user->email}}" name="email">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputName" class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" name="password" id="password" placeholder="***">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputName" class="col-sm-2 control-label">Re-Type Password</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" name="confirmation_password" id="confirmation_password" placeholder="***">
                    <span id="message"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputName" class="col-sm-2 control-label">Jenis Kelamin</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="jenis_kelamin" value="{{$user->jenis_kelamin}}">
                      <option value="Laki-laki" {{$user->jenis_kelamin == 'Laki-laki' ? 'selected' : ''}}>Laki-laki</option>
                      <option value="Perempuan" {{$user->jenis_kelamin == 'Perempuan' ? 'selected' : ''}}>Perempuan</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputName" class="col-sm-2 control-label">Nomor Telepon</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="no_telp" value="{{$user->no_telp}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputName" class="col-sm-2 control-label">Alamat</label>
                  <div class="col-sm-10">
                    <textarea name="alamat" class="form-control" rows="4">{{$user->alamat}}</textarea>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Simpan</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div> --}}
  </section>
@endsection
@section('js')
  <script type="text/javascript">
    $('#password, #confirmation_password').on('keyup', function () {
      if ($('#password').val() == $('#confirmation_password').val()) {
        $('#message').html('Password sama!').css('color', 'green');
      } else {
        $('#message').html('Password tidak sama!').css('color', 'red');
      }
    });
  </script>
@endsection

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');
Auth::routes(['register' => false, 'verify' => true]);

Route::get('/admin', function(){
    return redirect('/home');
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/home', 'DashboardController@index')->name('home');
    Route::get('profile', 'ProfileController@index');
    Route::put('profile/{id}', 'ProfileController@update');

    Route::group(['prefix' => 'pimpinan', 'namespace' => 'Pimpinan'], function(){
        Route::get('user', 'UserController@index');
        Route::get('user/tambah', 'UserController@create');
        Route::post('user/tambah', 'UserController@store');
        Route::put('user/password/{id}', 'UserController@changePassword');
        Route::put('user/aktif/{id}', 'UserController@changeActive');
        Route::put('user/nonaktif/{id}', 'UserController@changeInnactive');

        Route::get('konsumen', 'MasterController@indexKonsumen');
        Route::get('konsumen/download', 'MasterController@downloadKonsumen');
        Route::get('fotocopy', 'MasterController@indexFotocopy');
        Route::get('fotocopy/download', 'MasterController@downloadFotocopy');
        Route::get('penyewaan', 'MasterController@indexPenyewaan');
        Route::get('penyewaan/download', 'MasterController@downloadPenyewaan');
        Route::get('pengembalian', 'MasterController@indexPengembalian');
        Route::get('pengembalian/download', 'MasterController@downloadPengembalian');
        
    });

    Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function(){
        Route::get('konsumen', 'KonsumenController@index');
        Route::get('konsumen/tambah', 'KonsumenController@create');
        Route::post('konsumen/tambah', 'KonsumenController@store');
        Route::get('konsumen/{id}/edit', 'KonsumenController@edit');
        Route::put('konsumen/{id}/edit', 'KonsumenController@update');
        Route::delete('konsumen/delete/{id_konsumen}', 'KonsumenController@destroy');

        Route::get('fotocopy', 'FotocopyController@index');
        Route::get('fotocopy/tambah', 'FotocopyController@create');
        Route::post('fotocopy/tambah', 'FotocopyController@store');
        Route::get('fotocopy/{id}/edit', 'FotocopyController@edit');
        Route::put('fotocopy/{id}/edit', 'FotocopyController@update');
        Route::delete('fotocopy/delete/{id_fotocopy}', 'FotocopyController@destroy');

        Route::get('penyewaan/tambah', 'PenyewaanController@create');
        Route::post('penyewaan/tambah', 'PenyewaanController@store');
        Route::get('penyewaan', 'PenyewaanController@index');

        Route::post('penyewaan/{id}/pengembalian', 'PengembalianController@pengembalian');
        Route::get('pengembalian', 'PengembalianController@index');
    });
});

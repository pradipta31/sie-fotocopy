<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFotocopiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fotocopies', function (Blueprint $table) {
            $table->bigIncrements('id_fotocopy');
            $table->string('merk');
            $table->integer('tahun');
            $table->string('harga');
            $table->string('gambar');
            $table->integer('qty');
            $table->text('spesifikasi');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fotocopies');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenyewaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penyewaans', function (Blueprint $table) {
            $table->bigIncrements('id_penyewaan');
            $table->unsignedBigInteger('user_id')
                  ->foreign('user_id')->references('id_user')->on('users');
            $table->unsignedBigInteger('konsumen_id')
                  ->foreign('konsumen_id')->references('id_konsumen')->on('konsumens');
            $table->unsignedBigInteger('fotocopy_id')
                  ->foreign('fotocopy_id')->references('id_fotocopy')->on('konsumens');
            $table->date('tanggal_transaksi');
            $table->string('bukti_pembayaran');
            $table->integer('qty');
            $table->string('total');
            $table->text('keterangan');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penyewaans');
    }
}

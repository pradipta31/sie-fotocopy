<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'nama' => 'Pimpinan',
                'username' => 'pimpinan',
                'email' => 'pimpinan@gmail.com',
                'password' => Hash::make('123456'),
                'level' => 1,
                'no_telp' => '0809999900000',
                'status' => 1,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ],
            [
                'nama' => 'I Gede Pradipta Adi Nugraha',
                'username' => 'pradipta31',
                'email' => 'pradiptadipta31@gmail.com',
                'password' => Hash::make('123456'),
                'level' => 1,
                'no_telp' => '087861863842',
                'status' => 1,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ],
            // [
            //     'nama' => 'Admin',
            //     'username' => 'admin',
            //     'email' => 'admin@gmail.com',
            //     'password' => Hash::make('123456'),
            //     'level' => 2,
            //     'status' => 1,
            //     'created_at' => NOW(),
            //     'updated_at' => NOW()
            // ]
        ]);
    }
}

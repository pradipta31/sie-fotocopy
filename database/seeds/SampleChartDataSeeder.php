<?php

use Illuminate\Database\Seeder;
// use DB;
use Carbon\Carbon;

class SampleChartDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataPenyewaan = [];

        for ($i=0; $i < 12 ; $i++) {
            array_push($dataPenyewaan, [
                'user_id' => 1,
                'konsumen_id' => $i,
                'fotocopy_id' => $i,
                'tanggal_transaksi' => Carbon::now()->addMonths($i),
                'bukti_pembayaran' => 'bukti',
                'keterangan' => 'terang',
                'status' => 'active'
            ]);
        }

        DB::table('penyewaans')->insert($dataPenyewaan);

        $dataPengembalian = [];
        for ($i=0; $i < 12 ; $i++) {
            array_push($dataPengembalian, [
                'penyewaan_id' => 1,
                'tanggal_pengembalian' => Carbon::now()->addMonths($i),
                'keterangan' => 'terang',
                'status' => 'active'
            ]);
        }
        DB::table('pengembalians')->insert($dataPengembalian);
    }
}
